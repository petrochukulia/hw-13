let url = "https://swapi.dev/api/people/";

const data = fetch(url, { method: "Get" });

let data1 = data.then(
  (res) => res.json(),
  (er) => console.error(er)
);

data1.then((res1) => {
  showPeople(res1.results);
});

function showPeople(arr) {
  arr.forEach((element) => {
    const {
      name,
      birth_year,
      eye_color,
      gender,
      height,
      skin_color,
      homeworld,
    } = element;

    const cardDiv = document.createElement("div");
    cardDiv.classList.add("card");

    const nameDiv = document.createElement("div");
    nameDiv.classList.add("name");

    const birthDiv = document.createElement("div");
    birthDiv.classList.add("birth");

    const eyeDiv = document.createElement("div");
    eyeDiv.classList.add("eyes");

    const genderDiv = document.createElement("div");
    genderDiv.classList.add("gender");

    const heightDiv = document.createElement("div");
    heightDiv.classList.add("height");

    const skinDiv = document.createElement("div");
    skinDiv.classList.add("skin");

    const planetDiv = document.createElement("div");
    planetDiv.classList.add("planet");

    const btn = document.createElement("button");
    btn.setAttribute("type", "submit");

    document.body.append(cardDiv);

    nameDiv.innerText = name;
    birthDiv.innerText = birth_year;
    eyeDiv.innerText = eye_color;
    genderDiv.innerText = gender;
    heightDiv.innerText = height;
    skinDiv.innerText = skin_color;
    planetDiv.innerHTML = `<a href="${homeworld}">${homeworld}</a>`;
    btn.innerText = `Зберегти в браузері`;

    cardDiv.append(
      nameDiv,
      birthDiv,
      eyeDiv,
      genderDiv,
      heightDiv,
      skinDiv,
      planetDiv,
      btn
    );

    // Збереження в LocalStorage

    btn.onclick = () => {
      localStorage.setItem("name", name);
      localStorage.setItem("birth", birth_year);
      localStorage.setItem("eyes", eye_color);
      localStorage.setItem("gender", gender);
      localStorage.setItem("height", height);
      localStorage.setItem("skin", skin_color);
      localStorage.setItem("planet url", homeworld);
    };
  });
}
